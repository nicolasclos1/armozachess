export function generatePairings(roundPlayers, prevPairings, rounds) {

  let players = roundPlayers.slice()

  let usedPairings = [...prevPairings, ...prevPairings.map((emparejamiento) => {
    return {
      firstPlayer: emparejamiento.secondPlayer,
      secondPlayer: emparejamiento.firstPlayer,
    }
  })]

  players = players.slice().sort(() => Math.random() - 0.5).sort((a, b) => {
    if (b.points !== a.points) {
      return b.points - a.points
    } else {
      return b.elo - a.elo
    }
  })

  if (players.length % 2 !== 0) {
    const byePlayer = { id: 'BYE', name: 'BYE', surname: '', blacks: 0, whites: 0 }
    players.push(byePlayer)
  }

  const minPlayersForRound = (players) => {
    const minP = ((rounds * (rounds - 1)) / 2) + 1

    return (minP % 2 !== 0 ? (minP + 1) : minP)
  }


  const generatePosiblePairings = () => {

    const pairings = []

    for (let i = 0; i < players.length - 1; i++) {
      for (let j = i + 1; j < players.length; j++) {
        const newPairing = {
          firstPlayer: players[i],
          secondPlayer: players[j],
        }

        const emparejamientoPrevio = usedPairings.find(previo =>
          (previo.firstPlayer.id === newPairing.firstPlayer.id && previo.secondPlayer.id === newPairing.secondPlayer.id) ||
          (previo.firstPlayer.id === newPairing.secondPlayer.id && previo.secondPlayer.id === newPairing.firstPlayer.id)
        )

        if (!emparejamientoPrevio && players[i].id !== players[j].id) {
          pairings.push(newPairing)
          pairings.push({
            firstPlayer: newPairing.secondPlayer,
            secondPlayer: newPairing.firstPlayer,
          })
        }
      }
    }

    pairings.sort((a, b) => {
      if (a.firstPlayer.points !== b.firstPlayer.points) {
        return b.firstPlayer.points - a.firstPlayer.points
      } else {
        return b.firstPlayer.elo - a.firstPlayer.elo
      }
    })

    const posiblePairings = pairings.filter((pairing) => {
      const firstPlayer = pairing.firstPlayer

      const secondPlayer = pairing.secondPlayer

      if (firstPlayer.blacks >= firstPlayer.whites && secondPlayer.whites >= secondPlayer.blacks) {
        return pairing
      }
    })

    const selectedPairings = []
    const jugadoresEmparejados = new Set()

    let retryCount = 0;


    while ((selectedPairings.length < (players.length / 2)) && retryCount < 50) {
      function shuffle(array) {
        for (let i = array.length - 1; i > 0; i--) {
          const j = Math.floor(Math.random() * (i + 1));
          [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
      }
      shuffle(posiblePairings);

      console.log(retryCount)

      if (retryCount < 25) {
        posiblePairings.forEach(pairing => {
          if (
            !jugadoresEmparejados.has(pairing.firstPlayer.id) &&
            !jugadoresEmparejados.has(pairing.secondPlayer.id)
          ) {
            selectedPairings.push(pairing);
            jugadoresEmparejados.add(pairing.firstPlayer.id);
            jugadoresEmparejados.add(pairing.secondPlayer.id);
          }
        });
      } else {
        pairings.forEach(pairing => {
          if (
            !jugadoresEmparejados.has(pairing.firstPlayer.id) &&
            !jugadoresEmparejados.has(pairing.secondPlayer.id)
          ) {
            selectedPairings.push(pairing);
            jugadoresEmparejados.add(pairing.firstPlayer.id);
            jugadoresEmparejados.add(pairing.secondPlayer.id);
          }
        });
      }
      if (selectedPairings.length < (players.length / 2)) {
        // Si no se logró emparejar a todos, volvemos a generar pairings
        retryCount++;
        jugadoresEmparejados.clear();
        selectedPairings.length = 0;
      }

    }

    function comparePairings(a, b) {
      if (a.secondPlayer.id === 'BYE') {
        return 1
      } else if (b.secondPlayer.id === 'BYE') {
        return -1
      } else {
        return 0
      }
    }

    selectedPairings.forEach((emparejamiento, index) => {
      if (emparejamiento.firstPlayer.id === 'BYE') {
        selectedPairings[index] = {
          firstPlayer: emparejamiento.secondPlayer,
          secondPlayer: emparejamiento.firstPlayer,
        };
      }
    });

    function comparePairings(a, b) {
      if (a.secondPlayer.id === 'BYE') {
        return 1;
      } else if (b.secondPlayer.id === 'BYE') {
        return -1;
      } else {
        return 0;
      }
    }

    return selectedPairings.sort(comparePairings);
  }

  return generatePosiblePairings()

}