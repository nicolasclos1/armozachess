"use client"

import React from 'react'

import Link from 'next/link'

import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

import 'bootstrap/dist/css/bootstrap.min.css';

export default function ModalRoundPlayers({ handleClose, show, minPlayers, startTournament, selectedTournament }) {

    return (
        <Modal show={show} onHide={handleClose} className='modalContainer'>
            <Modal.Header className='modalHeader'>
                <Modal.Title>¿Desea Continuar?</Modal.Title>
            </Modal.Header>
            <Modal.Body className='modalBody'>
                <p>
                    La cantidad mínima de jugadores debe ser de <span>{minPlayers}</span> con la cantidad de rondas seleccionadas de lo contrario habrá emparejamientos repetidos. ¿Está de acuerdo?
                </p>
            </Modal.Body>
            <Modal.Footer className='modalFooter'>
                <Button className='closeModalBtn' onClick={handleClose}>
                    Cancelar
                </Button>
                <Link href={selectedTournament && `/torneos/${selectedTournament && selectedTournament.name ? selectedTournament.name.toLowerCase() : ''}`} className='updateModalBtn' onClick={() => {
                    startTournament()
                    handleClose()
                }}>
                    Comenzar Torneo
                </Link>
            </Modal.Footer>
        </Modal>
    )
}
