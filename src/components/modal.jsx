"use client"

import React, { useEffect, useState } from 'react'

import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

import 'bootstrap/dist/css/bootstrap.min.css';
import { updatePlayerNameAndSurnameInAllTournaments } from './fileOperations';

export default function ModalComponent({ handleClose, show, player, setPlayer }) {

  const [inputName, setInputName] = useState('')

  const [inputSurname, setInputSurname] = useState('')

  const handleInputName = (e) => {
    setInputName(e.target.value)
  }

  const handleInputSurname = (e) => {
    setInputSurname(e.target.value)
  }

  useEffect(() => {
    setInputName(player ? player.name : '')
    setInputSurname(player ? player.surname : '')
  }, [player])

  return (
    <Modal show={show} onHide={handleClose} className='modalContainer'>
      <Modal.Header className='modalHeader'>
        <Modal.Title>Modificar Nombre y Apellido</Modal.Title>
        <p>Los cambios aplicarán instanteneamente excepto que el torneo ya haya iniciado.</p>
        <p>Sea este el caso, los cambios aplicarán para futuro torneo. </p>
        <span>¡Ningún dato previo se perderá!</span>

      </Modal.Header>
      <Modal.Body className='modalBody'>
        <div className='modalInputsContainer'>
          <input placeholder='Nombre' defaultValue={player.name} type='text' onChange={handleInputName} />
          <input placeholder='Apellido' defaultValue={player.surname} type='text' onChange={handleInputSurname} />
        </div>
      </Modal.Body>
      <Modal.Footer className='modalFooter'>
        <Button className='closeModalBtn' onClick={handleClose}>
          Cancelar
        </Button>
        <Button className='updateModalBtn' onClick={() => {
          updatePlayerNameAndSurnameInAllTournaments(player, inputName, inputSurname)
          setPlayer({ ...player, name: setInputName, surname: setInputSurname })
          handleClose()
        }}>
          Modificar
        </Button>
      </Modal.Footer>
    </Modal>
  )
}
